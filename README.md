# GuildA - Functional Guild Annotator

## Installation
    git clone https://bitbucket.org/kucbd/guilda.git
It may take a while because of the large size of the Pfam database.
Please be patient, the repository is around 130Mb in size.

## Dependencies
- Python3
    - numpy 1.10.2
    - scikit-learn 0.19.1
    - matplotlib 1.5.1
- Gunzip

The program is made for linux/mac. For Windows users, we have an experimental Windows executable that may or may not work on your system.

A webserver is under construction.

## License
GuildA is free for academic users. Other users are requested to contact thomassp@snm.ku.dk

Prodigal is licensed under the GNU General Public License v3.

Hmmer is licensed under the 3-Clause BSD open source license.

Pfam is licensed under the GNU Lesser General Public License (v3 I think).

The experimental Windows version of GuildA was compiled using PyInstaller which is licensed under a custom GPL License.

## Overview of program and general usage
The general usage is:

    python guilda_pred.py -i <input-file(s)> -t <input-type> -o <output-folder>

The input files can be either in FASTA-format (nucleotide or protein) or in a tab-formatted PFAM annotation file. The "-t" option is the type of input ("n" for nucleotide fasta, "p" for protein fasta and "t" for tab-formatted Pfam-annotations). The "-o" option is the name of the output folder. It will be created if no folder exists. Note, however, if the folder already exists, the program will overwrite previously outputted data files.

For Windows users, please see the tutorial for Windows users.

---

## Tutorials
This section contains typical usage scenarios. In general, the output file you are looking for is found as the file "guild\_prediction.tab" in the specified output-folder. You can open this file in a spreadsheet program for a better overview of each guild and their probability. Note that you can have multiple inputs in tutorials 1-3.
### Tutorial 1: Nucleotide prediction
You have a genome "genome.fna". Type:

    python guilda_pred.py -i genome.fna -t n -o guild-output

### Tutorial 2: Protein prediction
You have a list of proteins from an organism called "proteins.faa". Type:

    python guilda_pred.py -i protein.faa -t p -o guild-output

### Tutorial 3: Pfam annotation prediction
You have already annotated your proteome with Pfam using your own Pfam database and Hmmer. You have the Hmmer tab-formatted output in a file called "pfam.tab". Type:

    python guilda_pred.py -i pfam.tab -t t -o guild-output

### Tutorial 4: IMG table annotation prediction
You have selected some organisms from IMG and have downloaded their PFAM annotations (see pictures in Tutorial 6 for how to do this). You file is called "img\_abundance.xls". Type:
    python guilda_pred.py -i img\_abundance.xls -t i -o guild-output

### Tutorial 5: Metagenomes
At the moment, GuildA cannot work directly on metagenomes. During my PhD we did work on a version that was trained on metagenomes, but it was not tested enough to be bundled with this release. Instead, we suggest you do metagenomic binning and run GuildA on each individual bin. We suggest using a program such as [MetaBAT](https://bitbucket.org/berkeleylab/metabat/src) or any other binning software (VizBin is probably the easiest to use, there's also GroopM, CONCOCT, MaxBin, Canopy).

When this is done, follow tutorial 1 for each of the bins. The predictor can take multiple input so if your bins are in a folder "bins", you can type:

    python guilda_pred.py -i bins/*.fna -t n -o bin-guilds

### Tutorial 6: Training a new Random Forest model on a new guild
The default Random Forest model is trained on 400 species spanning 25 guilds. If you want to add your own guild, you need to do two things. First, you want to update the file "data/GenomesNov22.csv" with your new organisms. Five organisms is the bare minimum, the more you add, the better it is. As of 2018 you have to manually look at the 400 organisms to annotate them with your new guild. We do however have a function that can automatically annotate the training set with your new guild using an iterative prediction process. We may implement this in a future version when we have done some more testing. If your guild is very rare, you may still get good results even if you do not re-annotate the 400 organisms in the training set.

When this is done, you need to annotate your added organisms with PFAM. You can only do this using the [IMG database](https://img.jgi.doe.gov/cgi-bin/m/main.cgi). Go to the "Find Genomes" and find the genomes you have annotated, and add them to the cart. Then, in the menu click on "Compare Genomes" --> "Abundance Profiles" --> "Overview (All Functions)". In the output type click on "Matrix" and in Function click on "Pfam". Click Go, and when the page has finished loading, download the table in the link "Download tab-delimited file for Excel". Let's say you save it as "new\_img\_organisms.csv". It's ok if the names from IMG are a bit dissimilar to the names in the data/guilds2018JULY.csv. Guilda\_train.py compares the names and if they are very similar they will be grouped as one, e.g. "Micro Organism spp. 1" will match with "Microorganism s1". In case the names are too dissimilar, the program will exclude them from the training.

![Getting IMG Pfam annotations 1](data/IMG1.png?raw=true "IMG1")
![Getting IMG Pfam annotations 2](data/IMG2.png?raw=true "IMG2")

You can now train a random forest model by writing:

    python guilda_train.py -i final_abundance.tab new_img_organisms.csv -g data/guilds2018JULY.csv -o OUTPUT-FOLDER

After training, your new model will be found in OUTPUT-FOLDER/rfmodel.pkl

You can now predict the presence of your guild in new organisms like this:

   python guilda_pred.py -i nucleotides.fna -t n -o PREDICTION -m OUTPUT-FOLDER/rfmodel.pkl

Keep in mind, however, the quality of the classifier before you trust your results. During training, the guilda\_pred.py script will output several metrics that will tell you how well the classifier performs. Look at the AUC and MCC scores to determine if you can trust your predictions. Also look at the Youden's J statistic for an idea of where to put your probability threshold.

### Tutorial 7: Windows users
This tutorial is divided in two: Predicting guilds and training a new random forest on new guilds.

Download the windows portable folder from this dropbox link and unzip the folder.
#### 7.1: Prediction
NOTE: As of July 2018 this does not work. We cannot compile hmmer on windows machines. If anyone has a working cygwin hmmer binary, please notify us.

Double-click the "guilda\_pred.exe" file and follow the on-screen messages. Copy the full path to your file (right-click, properties, copy path+name) and paste it when the program prompts for the input file.

Press Enter when you have pasted the path, and then type "n" for nucleotide, "p" for protein and "t" for tab-formatted Pfam annotations depending on your input data.

Finally, type the name of the output folder. If you just write a name, the folder will be created in the GuildA folder.

The predictions will be found in the output-folder under the name "guild\_prediction.tab" which you should be able to open in Excel or other spread sheet programs.

#### 7.2: Training
Under construction...


---
## Known Issues
- Windows version is a bit buggy, and may freeze.
    - Perhaps add a GUI.
    - Windows executable currently does not work..
- A negative sample (i.e. a sample with no PFAM annotations) will give some probabilities.
    - Fix by adding a sample with no PFAMs as a negative sample in the training set.
- The guilda\_train.py script does not give error messages if there are errors in the input, but just crashes.
    - Fix by validating input
- Some guild models such as the anammox guild may have been overfitted.
    - Add more variation in the training set for those guilds.

## Reference
Al-Nakeeb, K., Fowler, J., Smets, B.F., & Sicherits-Ponten, T. **GuildA: Functional classification of genomic data using machine learning.** *Manuscript in preparation*.


