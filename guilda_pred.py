#!/usr/bin/env python
import numpy #data structure
import matplotlib.pyplot as plt #Plotting machine learning plots
import sklearn #Machine learning
import argparse #Parsing input arguments
import importlib.util #Checking missing packages
import sys,os #pathnames (os) and exiting (sys)
import subprocess #Running commands in shell
import datetime #For logfile time stamp

#Setting up paths
spath = os.path.dirname(os.path.realpath(sys.argv[0]))+"/"
### HERx
#spath = "."
## Pointing at the right binaries
if sys.platform == "darwin": #if mac
    bpath = spath+"binaries/darwin/"
else: #assuming linux, not windows
    bpath = spath+"binaries/linux/"
scripts = spath + "scripts/" #scripts path
dpath = spath + "data/" #data path


#Arguments
#### Getting command line arguments
parser = argparse.ArgumentParser(prog="guilda.py",usage="python guilda.py -i <genome.fna>/<proteins.faa>/<pfam_annotation.tab> -o Results",\
         description='''Version 1 (2018)
Predicts functional guilds in genomic data:''',formatter_class=argparse.RawDescriptionHelpFormatter,
     epilog='''
----------------------------------------------
Input description
----------------------------------------------
Guilda works for the following input types:
- Nucleotides (-t n)
  --> Runs Gene calling (Prodigal), Pfam annotation (hmmsearch) and Random Forest prediction
- Proteins (-t p)
  --> Runs Pfam annotation (hmmsearch) and Random Forest prediction
- Pfam annotation table(s) (-t t)
  --> Runs Random Forest prediction
- IMG abundance table (-t i) #NOTE! Works only for single IMG file at a time
  --> Runs Random Forest prediction

NOTE ABOUT OUTPUT:
The output is a tab-separated file (.tab) that
can be read from Excel or similar. If you have multiple
input files, each file will be on a row in the
tab-file.

See https://bitbucket.org/kosaidtu/guilda for full documentation.'''.format("hej"))
parser.add_argument("-i", metavar="FILE",help="Input file(s)",nargs="+",required=True)
parser.add_argument("-t", metavar="TYPE",choices=['n','p','t','i'],help="Type of input data. Use 'n' for nucleotide, 'p' for peptide/proteins, 't' for Pfam annotated tab-files and 'i' for IMG abundance tables. Do not mix types in the input.",type=str,nargs=1,required=True)
parser.add_argument("-o", metavar="NAME",help="Folder for output. (Required)",nargs=1,required=True)
parser.add_argument("-z", metavar="INT",help="Number of threads. Default=2",type=int,default=2)
parser.add_argument("-p", metavar="FILE",help="Specify custom PFAM HMM file instead of using default.",nargs=1)
parser.add_argument("-m", metavar="FILE",help="Specify custom Random Forest model instead of using default.",nargs=1)
#parser.add_argument("--path",help="Use programs in $PATH instead of included pre-compiled binaries",action="store_true")
#parser.add_argument("--delete",help="Delete BAM-files and other large temporary files.",action="store_true")

args = parser.parse_args()
#args = parser.parse_args("-i data_pred/1.faa data_pred/2.faa -o 3433 -f p".split()) #REMOVE THIS TEST PLZ
#args = parser.parse_args("-i data_pred/1.fna data_pred/2.fna -o 3433 -f n".split()) #REMOVE THIS TEST PLZ
#args = parser.parse_args("-i data_pred/tab.tab -o 3433 -f t".split()) #REMOVE THIS TEST PLZ

print("Arguments: ",args)


#Binaries
try:
    if args.path: #Use local software instead
        bpath = ""
except:
    pass

#Deal with custom PFAM installation
if args.p:
    pfam=args.p[0]
    #Later: add validation for correct file format here
else:
    pfam=dpath+"Pfam-reduced.hmm.gz"


#output folders
#shutil.rmtree(opath) #for debugging. Didnt want to delete the same folder again and again...
opath = args.o[0]
if not os.path.exists(opath):
    os.mkdir(opath)
    os.mkdir(opath+"/data")
#    os.mkdir(opath+"/plots")
    os.mkdir(opath+"/log")    
else:
    print("Warning! Output folder already exists! Will overwrite existing files.")
#    sys.exit("Output folder already exists")
out = opath+"/"


#Write to logfile
def writelogfile(logfile):
    with open("{0}log/logfile.txt".format(out),"a") as fout:
        s = fout.write(logfile+"\n")

writelogfile(str(args))
# Checking if required packages are installed
package_name = ["sklearn","matplotlib","numpy"]
packages_ok = 1
for pack in package_name:
    spec = importlib.util.find_spec(pack)
    if spec is None:
        print("Error: {0} is not installed... ".format(pack))
        print("Try 'pip install {0}' or 'conda install {0}' and re-run.".format(pack))
        writelogfile("Error: {0} is not installed... ".format(pack))
        writelogfile("Try 'pip install {0}' or 'conda install {0}' and re-run.".format(pack))
        packages_ok = 0
if packages_ok == 0:
    sys.exit("Please install missing Python packages.")

### HERx
if args.m:
    try:
        a = sklearn.externals.joblib.load("{0}".format(args.m[0]))
    except:
        writelogfile("Could not load custom model: {0}".format(args.m[0]))
        sys.exit("Could not load custom model: {0}".format(args.m[0]))
else:
    a = sklearn.externals.joblib.load("{0}data/rf2018.pkl".format(spath))

feature_names,ParseTheseGuilds,ModelList = a[0],a[1],a[2]
if type(feature_names) == numpy.ndarray:
    feature_names_backup = feature_names
    feature_names = feature_names[0].tolist()
else:
    feature_names = feature_names_backup
    feature_names = feature_names[0].tolist()

#Running terminal scripts based on input type
runcmdD = {"n":("Gene calling",[0,0]),
          "p":("Pfam annotation",[1,0]),
          "t":("Guild prediction",[1,1])} #nucleotide, protein, 
runorder = ["n","p","t"]
stat2message = {}
i = 0
for el in runorder:
    stat2message[i] = runcmdD[el][0]
    i+=1

#Get runcommand
def getruncmd(inputfile):
    return runcmdD[inputfile[0]][1].copy() #Force user to tell input type



#OLD: Takes into account not to overwrite existing files
#if args.t: #if type specificed
#    runcmd = runcmdD[args.t[0]][1] #Force user to tell input type
#else: #Assume nucleotide
#    runcmd = runcmdD["n"][1] #Determine input from extension
stat = 0 #Where we start
logfile = "" #Program log
currentdatetime = datetime.datetime.now().isoformat()
currentdatetime_string = "{0}\n{1}\n{0}\n".format("-"*len(currentdatetime),currentdatetime)
writelogfile(currentdatetime_string)
with open("{0}log/programs.output".format(out),"a") as fout:
    fout.write(currentdatetime_string)

#errfile=open("{0}log/programs.output".format(out),"a")

#Runs commands in shell
def doit(cmd,message,force=0):
 #print(runcmd)
 #print(stat)
 logfile = ""
 if runcmd[stat] == 1 and force ==0:
  logfile += "SKIPPED "+message+"....\n"
 else:
  logfile += "DOING "+message+"....\n"
  logfile += "#COMMAND: "+cmd+"\n"
  try:
   with open("{0}log/programs.output".format(out),"a") as errfile:
    hej = subprocess.check_call(cmd,shell=True,stderr=errfile,stdout=errfile)
   #hej = subprocess.check_call(cmd,shell=True)
   print("#COMMAND: "+cmd)
   #print("Status (0 is good): {0}".format(hej))
   if force == 0:
    logfile += "SUCCESS!\n"
    runcmd[stat] = 1
    writelogfile(logfile)
    #pickle.dump(checkpoint,open("log/checkpoints.p","wb"))
  except Exception as e:
   print(e)
   writelogfile("FAIL!\n"+logfile)
   writelogfile(e)
   errfile.close()
   sys.exit("****** Failed "+message+".. Exit.\nDid you check that your -f corresponds to your input type?")


#myoutput = args.o[0]
#myoutput = "./" #HER!! x
myoutput = out
filelist = [] #Final tab-file(s)
k2filename = {}

#### METAGENOME TEST. JGI MODULE
if args.t[0] == "i": #IMG FILE
    Xmeta = []
    Xm = []
    Xmeta_set = set()
    Xmeta_dict = {}
    a = 0
    if len(args.i) > 1:
        writelogfile("ERROR! At the moment, only 1 input IMG can be given, please split up your predictions")
        sys.exit("ERROR! At the moment, only 1 input IMG can be given, please split up your predictions")
    for img_file in args.i: #WILL GIVE ERROR IF THERE ARE MULTIPLE args.i
        Xm = []
        with open(img_file) as fid:
            line = fid.readline()
            Lmeta = line.rstrip().split()[2:]
            line = fid.readline().rstrip()
            while line:
                pf = line.split("\t")[0]
                Xmeta_set.add(pf)
                Xmeta_dict[pf] = a
                a += 1
                Xm.append(line.rstrip().split("\t")[2:])
                line = fid.readline().rstrip()
        Xm = numpy.matrix(Xm).transpose()
        #Xmeta = [ [False for x in range(len(feature_names))] for xx in range(Xm.shape[0])  ]
        Xmeta.extend([[False]*len(feature_names) for i in range(Xm.shape[0])])

    for i in range(len(feature_names)):
        el = feature_names[i]
        if el in Xmeta_set:
            for ii in range(Xm.shape[0]):
                if Xm[ii,  Xmeta_dict[el]    ] != "0":
                    Xmeta[ii][i] = True
                #Xmeta.append(True)
        #else:
            #Xmeta.append(False)
    if len(Xmeta) == 1:
        Xmeta = numpy.array(Xmeta).reshape(1,-1)
    else:
        Xmeta = numpy.matrix(Xmeta)
    Xmeta.astype(numpy.bool)
    #### END OF METAGENOME TEST
    Xsample = Xmeta
    
    ### REMOVING DUPLICATES IN TAB DATA
    Lmetaset = set()
    badindex = []
    Lmeta2 = []
    i = 0
    for el in Lmeta:
        if el not in Lmetaset:
            Lmetaset.add(el)
            Lmeta2.append(el)
        else:
            el = el+"---"+str(i)
            i += 1
            print(el)
            Lmetaset.add(el)
            Lmeta2.append(el)
            #badindex.append( Lmeta.index(el) ) ###REMOVE PREVIOUS 2 AND UNCOMMENT THIS AND THE NEXT 2
            

    #Xmeta = numpy.delete(Xmeta,badindex,0) #OVERWRITING XMETA AND LMETA
    filelist = Lmeta2 
    k2filename = {filelist[_]:filelist[_] for _ in range(len(filelist))}

else: #NOT IMG FILE
    for files in args.i:
        stat = 0
        runcmd = getruncmd(args.t)
        myfile = os.path.basename(files)
        #Gene calling
        cmd = "{0}prodigal -i {1} -a {2}data/{3}.protein.faa -o /dev/null -q".format(bpath, files, out,myfile )
        doit(cmd,stat2message[stat])
        
        #Pfam annotation
        if not os.path.exists("{0}data/{1}.protein.faa".format(out,myfile)):
            cmd = "ln -s {0} {1}data/{2}.protein.faa".format(os.path.realpath(files),out,myfile)
            doit(cmd,"Fixing links",force=1)
        elif os.stat("{0}data/{1}.protein.faa".format(out,myfile)).st_size == 0:
            with open("{0}data/{1}.protein.faa".format(out,myfile),"w") as fout:
                pass
        stat += 1

        if os.stat("{0}data/{1}.protein.faa".format(out,myfile)).st_size == 0:
            with open("{0}data/{1}.pfam.tab".format(out,myfile),"w") as fout:
                pass
        else:
            cmd = "{0}hmmsearch -o /dev/null --tblout {2}data/{3}.pfam.tab --acc --cut_ga --cpu {1} {4} {2}data/{3}.protein.faa".format(bpath,args.z,out,myfile,pfam)
            doit(cmd,stat2message[stat])

        #Machine learning
        if not os.path.exists("{0}data/{1}.pfam.tab".format(out,myfile)):
            cmd = "ln -s {0} {1}data/{2}.pfam.tab".format(os.path.realpath(files),out,myfile)
            doit(cmd,"Fixing links",force=1)
        stat += 1
        filelist.append("{0}data/{1}.pfam.tab".format(out,myfile))
        k2filename["{0}data/{1}.pfam.tab".format(out,myfile)] = files

    import re
    import glob #Remove this fam
    rpat = re.compile("PF\d{5}\.\d+")
    Xsample_d = {}
    #### HER x
    #filelist = ["./data_pred/tab.tab"]
    #filelist = glob.glob("./data_pred/*tab")
    for myfile in filelist:
        with open(myfile) as fid:
            Xsample_d[myfile] = {}
            for line in fid:
                if line[0] == "#":
                    continue
                if not rpat.findall(line):
                    continue
                #pf = "{0}{1}".format("pfam",line.split()[3].split(".")[0][2:])
                pf = "{0}{1}".format("pfam",rpat.findall(line)[0].split(".")[0][2:])
                Xsample_d[myfile][pf] = Xsample_d[myfile].setdefault(pf,0) + 1

    Xsample = []
    for myfile in filelist:
        tempL = []
        for pf in feature_names: #MAYBE CHANGE to WITHOUT [0]
            if pf in Xsample_d[myfile]:
                tempL.append(True)
            else:
                tempL.append(False)
        Xsample.append(tempL)
    if len(Xsample) == 1:
        Xsample = numpy.array(Xsample,dtype=numpy.bool).reshape(1,-1) 
    else:
        Xsample = numpy.matrix(Xsample,dtype=numpy.bool)

writelogfile("Running Random Forest predictions...")

Xsample_D = {}
Xsample_scores = []
for i in range(len(ParseTheseGuilds)):
    Xsample_scores.append(  ModelList[i].predict_proba(Xsample))

for i in range(len(Xsample_scores)):
    for ii in range(len(filelist)):
        Xsample_D.setdefault(filelist[ii],[]).append(   Xsample_scores[i][ii,1]      ) #predict_proba
        
#### HER x
#with open("./data_pred/_PRED.txt","w") as fout:
with open("{0}guild_prediction.tab".format(out),"w") as fout:
    print("\nSample\t{0}".format( "\t".join(ParseTheseGuilds)        ))
    fout.write("Sample\t{0}\n".format( "\t".join(ParseTheseGuilds)        ))
    for k in sorted(Xsample_D):
        myprint = k
        myprint = os.path.splitext(os.path.basename(k2filename[k]))[0]
        #for kk in Xsample_D[k]:
            #myprint += "\t{0}".format("\t".join([str(x) for x in kk]))
        myprint += "\t{0}".format("\t".join([str(round(x,3)) for x in Xsample_D[k]]))
        print(myprint)
        s = fout.write("{0}\n".format(myprint))

#errfile.close()

writelogfile("DONE!")

