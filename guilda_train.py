import sys,os,re, html, urllib, requests, resource,gzip, difflib, random
import argparse
import numpy
import codecs
import shutil
import matplotlib.pyplot as plt

import warnings
warnings.filterwarnings("ignore", category=RuntimeWarning)


#Setting up paths
spath = os.path.dirname(os.path.realpath(sys.argv[0]))+"/"
### HERx
#spath = "."
## Pointing at the right binaries
if sys.platform == "darwin": #if mac
    bpath = spath+"binaries/darwin/"
else: #assuming linux, not windows
    bpath = spath+"binaries/linux/"
scripts = spath + "scripts/" #scripts path
dpath = spath + "data/" #data path



#Setting up string-vars for various output-files
S_omitted_pfams = "" #Omitted Pfams (features not found in the Pfam-A.hmm file)
S_problematic = "" #Potential organisms when adding guild, positive/negative
S_metrics  = "" #The performance metrics in tab-format
S_omitted_orgs = "" #Organisms from guild-excel not found in abundance table(s)
S_guilds = "" #Guild overview and number of members in each
S_log = "" #General log

#Remove this when deploying
if os.path.exists("TEST"):
    shutil.rmtree("TEST")
    print("Removing TEST....")


#Check how much RAM is being used at any given time. For debugging purposes
def ram_usage():
    return "RAM usage: {0} MB".format(round(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1000000))

#Command-line input
parser = argparse.ArgumentParser(prog="guilda_train",usage="python guilda_train.py -i ...",description="Version 1 (2018)\nScreens NGS data for guilds.",formatter_class=argparse.RawDescriptionHelpFormatter,
epilog='''
----------------------------------------------
Tutorial
----------------------------------------------
See https://bitbucket.org/kosaidtu/guilda for full documentation.''')
parser.add_argument("-i", metavar="FILE",help="IMG csv-file(s) of organisms with PFAM annotations.", nargs="+",required=True)
#parser.add_argument("-t", metavar="FILE",help="Tab-formatted Pfam annotations of organisms. WARNING: Make sure the files have the form <ORGANISM.tab> and that the ORGANISM name is exactly as it is in the Guild-annotation file.", nargs=1,required=True)
parser.add_argument("-g", metavar="FILE",help="Guild annotations in csv format", nargs="+")
parser.add_argument("-o", metavar="NAME",help="Output folder",nargs=1,required=True)
#parser.add_argument("-t", metavar="INT",help="Number of threads. Default=2",type=int,default=2)
#parser.add_argument("--path",help="Use programs in $PATH instead of included pre-compiled binaries",action="store_true")

#args = parser.parse_args('-i data/abundance_pfam_70608.tab -g ./data/GenomesNov22.csv ./data/yoyoyo.csv -o TEST'.split())
args = parser.parse_args('-i data/final_abundance.tab -g ./data/guilds2018JULY.csv -o TEST'.split())
#args = parser.parse_args('-i data/abundance_pfam_70608.tab -g ./data/guilds2018JUNE.csv -o TEST'.split())


#args = parser.parse_args() #Uncomment this, and delete above when going live..

print(args)

#Preparing output-folder
opath = args.o[0].rstrip("/") + "/"
log_log = ""
log_warn = ""
log_err = ""
log_excluded = ""
if not os.path.exists(opath):
    try:
        os.mkdir(opath)
        os.mkdir(opath+"tmp")
        os.mkdir(opath+"models")
        os.mkdir(opath+"plots")
        os.mkdir(opath+"log")
    except Exception as e:
        sys.exit("Error! Cannot create output-folder!\n{0}".format(e))
else:
#    print("Warning! Output folder already exists!\n Will overwrite existing models")
    sys.exit("Output folder already exists")

#Note down the ram usage
log_log += "{0}\n{1}\n".format(args,ram_usage())
S_log += "{0}\n{1}\n".format(args,ram_usage())
#print(ram_usage())

#Writing log files
def writelog(s,fname,opath):
    with open("{0}/{1}".format(opath,fname),"a") as f:
        s = f.write(s)


# Loading data from Guilds + annotation
## Abundance table

#New, takes multiple input:
myfilelist = args.i
feature_names2 = {}
feature_names_extended2 = {}
L = []
if len(args.i) > 1:
    for myfile in myfilelist:
        if not os.path.exists(myfile):
            sys.exit("Could not find file: {0}".format(myfile))
        with open(myfile) as fid: ### JUNE 2018
            s = fid.readline()
            L.extend(s.split()[2:])
            LL = s.split()[2:]
            line = fid.readline().rstrip()
            S = [] #data
            feature_names = []
            feature_names_extended = []
            fD = {}
            SB = []
            while line:
                S.append(line.split("\t")[2:])
                feature_names.append(line.split("\t")[0])
                feature_names_extended.append(line.split("\t")[1])
                fD[feature_names[-1]] = feature_names_extended[-1]
                Stemp = []

                for pc in line.split("\t")[2:]:
                    if int(pc) != 0:
                        #Stemp.append(float(1))
                        Stemp.append(True)
                    else:
                        #Stemp.append(float(0))
                        Stemp.append(False)
                SB.append(Stemp)
                for i in range(len(LL)):
                    s = feature_names2.setdefault(line.split("\t")[0],dict())
                    feature_names2[line.split("\t")[0]][LL[i]] = Stemp[i]
                line = fid.readline().rstrip()

    L = list(set(L))
    pfamlist = feature_names2.keys()
    SB = []
    for pfam in pfamlist:
        Stemp = []
        for org in L:
            Stemp.append(feature_names2[pfam][org])
        SB.append(Stemp)
    feature_names = list(pfamlist)
    feature_names_extended = [ fD[_] for _ in feature_names   ]
else: #OLD! Takes single input, but super fast
    with open(args.i[0]) as fid:
        s = fid.readline()
        L = s.split()[2:]
        line = fid.readline().rstrip()
        S = [] #data
        feature_names = []
        feature_names_extended = []
        fD = {}
        SB = []
        while line:
            S.append(line.split("\t")[2:])
            feature_names.append(line.split("\t")[0])
            feature_names_extended.append(line.split("\t")[1])
            fD[feature_names[-1]] = feature_names_extended[-1]
            Stemp = []
            for pc in line.split("\t")[2:]:
                if int(pc) != 0:
                    #Stemp.append(float(1))
                    Stemp.append(True)
                else:
                    #Stemp.append(float(0))
                    Stemp.append(False)
            SB.append(Stemp)
            line = fid.readline().rstrip()

#L
#SB
#feature_names
#feature_names_extended


## Guild annotations
if not args.g:
    argsg = [dpath+"guilds2018JULY.csv"]
else:
    argsg = args.g

L2 = []
y = []
for guildfile in argsg:
    with codecs.open(guildfile,encoding="utf-8") as fid:
    #L2 = []
    #y = []
        line = fid.readline()
        line = fid.readline()
        while line:
            LS = line.rstrip().split(";")
            if LS:
                temp = []
                for i in LS[2:]:
                    if i and i[0] != "*":
                        temp.append(     re.sub("\*.+","",i).lower()   )
                y.append(temp)
                L2.append(LS[2])
            line = fid.readline()

## Fixing row/columns, deleting duplicate entries in abundance tab
seen_before = {}
seen_beforeL = []
k = -1
minusk = 0
SBnp = numpy.matrix(SB[:]).transpose()
SB2 = SB[:]
LB2 = numpy.array(L[:])
deleterows = []
for i in L:
    k += 1
    if i in seen_before:
#        print("****",k,seen_before[i])
        seen_beforeL.append(k)
        if SBnp[k].sum() > SBnp[seen_before[i]].sum():
            deleterows.append( seen_before[i] )
            #SBnumpy.pop(seen_before[i]-minusk)
            #LB2.pop(seen_before[i]-minusk)
        else:
            deleterows.append( k )
            #SBnumpy.pop(k-minusk)
            #LB2.pop(seen_before[i]-minusk)
        minusk += 1
    else:
        seen_before[i] = k
#print("Seen before {0}".format(seen_beforeL))

SB2 = numpy.delete(SBnp,deleterows,0)
SB2.astype(bool)
LB2 = numpy.delete(LB2,deleterows,0)
#print(SB2.shape,LB2.shape)


## Mapping Guild-annotations with abundance tab
#  (also, deals with misspelling between two documents etc)
from operator import itemgetter
#sorted(unsorted_list, key = itemgetter(3))
EXCLUDED = []
Lset = set([x.lower() for x in LB2])
Llower = [x.lower() for x in LB2]
import re,difflib
L2_ny = []
c = 0
cc = 0
L2_d = {}
seqdiffL = []
L_index = []
y2 = []
X = numpy.matrix(SB2[0])
#for el in L2:
for iel in range(len(L2)):
    el = L2[iel]
    el = el.lower() #This and next lines fixes spaces and common special chars
    el = re.sub("\s|\.|\-|\/|\,","_",el)
    el = re.sub("\(|\)","_",el)
    el = re.sub("_+","_",el)
    if el in Lset:
        L2_ny.append(el)
        L2_d[el] = el
        L_index.append(  Llower.index(el)  )
        y2.append(  y[iel]  )
        #X.concatenate( SB2[ Llower.index(el)   ]   )
        X = numpy.concatenate( (X,SB2[ Llower.index(el)   ]),axis = 0   )
    elif el not in Lset:
        c += 1
        yo = []
        for i in Lset:
            yo.append([el,i,difflib.SequenceMatcher(None,el,i).ratio()])
        best = sorted(yo, key = itemgetter(2),reverse=True)[0]
        if best[2] >= 0.85:
            L2_ny.append(best[1])
            L2_d[best[0]] = best[1]
            seqdiffL.append(  best    )
            L_index.append(  Llower.index(best[1])  )
            y2.append(  y[iel]  )
            #X.append( SB2[ Llower.index(best[1])   ]   )
            X = numpy.concatenate( (X,SB2[ Llower.index(best[1])   ]),axis = 0   )
            #print("{0}\n{1}\n****".format(el,best[1]))
        elif best[2] >= 0.8 and ("reannotation" in best[1] or "dsm" in best[1]):
            L2_ny.append(best[1])
            seqdiffL.append(  best    )
            L_index.append(  Llower.index(best[1])  )
            y2.append(  y[iel]  )
            #X.append( SB2[ Llower.index(best[1])   ]   )
            X = numpy.concatenate( (X,SB2[ Llower.index(best[1])   ]),axis = 0   )
            #print("{0}\n{1}\n****".format(el,best[1]))
        else:
            #L2_ny.append("UNKNOWN")
            #print(el)
            log_excluded += "{0}\n".format(el)
            seqdiffL.append(  [best[0],"UNKNOWN",best[2]]    )
            EXCLUDED.append(yo)
            cc += 1
#print("Found {0}, excluded {1}".format(c,cc))
log_excluded += "Found {0}, excluded {1}".format(c,cc)
S_omitted_orgs += "Found {0} organisms with name mismatches\n".format(c)
S_omitted_orgs += "Excluded {0} organisms from training, because of no matches:\n".format(cc)
X = numpy.delete(X,0,0) # Training data

## Which organisms are NOT found in the abundance table?
for yo in EXCLUDED:
    #print("---------------\n{0}".format(yo[0][0]))
    S_omitted_orgs += "---------------\n* {0}".format(yo[0][0])
    for yoo in sorted(yo, key = itemgetter(2),reverse=True)[0:3]:
    #    print(yoo[1],yoo[2],"\n",end="")
        S_omitted_orgs += "{0}\t{1}\n".format(yoo[1],yoo[2])
    #print("")
    S_omitted_orgs += "\n"

#print(S_omitted_orgs)



# Deduplicating, removing pfams with all 1's or 0's
#2nd - Backup X into Xddedupl
#1st - Restore X after running this paragraph
try:
    X = Xddedupl
    feature_names = feature_names_ddedupl
    feature_names_extended = feature_names_ext_ddedupl
except:
    Xddedupl = X
    feature_names_ddedupl = feature_names
    feature_names_ext_ddedupl = feature_names_extended

ddedupl = []
for i in range(X.shape[1]):
    if X[:,i].sum() >= X.shape[0]:
        ddedupl.append(i)
    elif X[:,i].sum() < 2: #0 or 1 ###CHANGE TO elif AND UNCOMMENT ABOVE TO REMOVE "HOUSEHOLD"
        ddedupl.append(i)

ddedupl = numpy.array(ddedupl)
X = numpy.delete(X,ddedupl,1)
feature_names = numpy.delete(numpy.array(feature_names).reshape(1,-1),ddedupl,1)
feature_names_extended = numpy.delete(numpy.array(feature_names_extended).reshape(1,-1),ddedupl,1)
#print(len(ddedupl))
#print("Final shape: ",X.shape)
S_guilds += "#No. of removed features: {0} (all 0's or 1's)\n".format(len(ddedupl))

S_guilds += "#Final training set size:\n#{0} Organisms with guilds and {1} features ({0}x{1} matrix)\n#\n".format(X.shape[0],X.shape[1])
S_guilds += "#List of guilds:\n{0}\n".format("-"*20)

#### PARSETHESE GUILDS
myguilds = {}
k = 0
for i in y2:
    for ii in i[1:]:
        myguilds.setdefault(ii,set()).add(k)
    k += 1
myguildsN = {k:len(myguilds[k]) for k in myguilds   }
ParseTheseGuilds = []
import operator
for i in sorted(myguildsN.items(),key=operator.itemgetter(1),reverse=True):
#    print(i[0],i[1])
    S_guilds += "{0}\t{1}\n".format(i[0],i[1])
    if i[1] >= 5:
        ParseTheseGuilds.append(i[0])

#print("Sum of all individual guilds: {0}".format(sum( [x for x  in myguildsN.values()] )))
S_guilds += "Sum of all individual guilds: {0}".format(sum( [x for x  in myguildsN.values()] ))

# Threshold Y index
def get_threshold(fpr,tpr,threshold):
    #q1,q2 = (0,1)
    #myidx = []
    #for p1,p2 in zip(fpr,tpr):
    #    myidx.append( numpy.sqrt( (q1-p1)**2 + (q2-p2)**2    ) )
    #print(myidx)
    #return threshold[numpy.argsort(myidx)[0]]
    J = []
    for FPR,TPR in zip(fpr,tpr):
        sens = TPR
        spec = 1-FPR
        J.append(sens+spec-1)
    #print(J)
    myidx = numpy.argsort(J)[-1]
    #print(myidx)
    return round(threshold[myidx],2)
mythresholds = {}

# Model
#%%time

#MAIN TRAINER
from sklearn import ensemble #Random Forest
from sklearn.model_selection import cross_val_predict
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.preprocessing import OneHotEncoder
from sklearn.metrics import matthews_corrcoef
import pprint,random
from scipy import interp

#New models
from sklearn import svm
from sklearn.naive_bayes import GaussianNB
from sklearn.neural_network import MLPClassifier

RFkernel = "linear"
RFC = 1.0
RFprobability = True
RFdegree = 3
RFrandom_state = 21
RFn_jobs=-1

from sklearn import metrics
#numpy.random.seed(10)
#random.seed(100)


#Converting input matrix to numpy matrix
#SB2 = numpy.matrix(SB)
guild = 'nitrogen fixation' #DELETE THIS FAM
#guild = "dehalogenation"
#guild = "anaerobic aromatic hydrocarbon degradation"
ParseTheseGuilds2 = sorted(ParseTheseGuilds)
ParseTheseGuilds2 = ["cellulose degradation","nitrogen fixation"]
#ParseTheseGuilds2 = [guild] #DELETE THIS TOO FAM
YO = []

#List with models to pickle
ModelList = []
#X = Xddedupl

myguilds2 = myguilds
#Model paramters
RFn_estimators=100 #500
RFbootstrap=True #True ?
RFmin_samples_split=2 #3
#RFmax_features=int(numpy.sqrt(X.shape[1])) #"auto"
RFmax_features=X.shape[1] #"auto"
RFmax_features=None #"auto"
#RFmax_features="auto" #"auto"
#RFmax_features=128
RFwarm_start=False
RFmax_depth= 3 # --> None (3)
#RFmax_depth = None
RFmax_leaf_nodes=None #2 --> None (3)
RFmin_samples_leaf=1 #1 (2)
RFn_jobs=-1
RFrandom_state=10
Guildindice = []
Performances  = []

CV = 5
#Colors for ROC:
from scipy import interp
import pylab
NUM_COLORS_L = []
cm = pylab.get_cmap('gist_rainbow')
for i in range(CV):
    NUM_COLORS_L.append(cm(1.*i/CV))
Tscores = []

skf = StratifiedShuffleSplit(n_splits=CV,random_state=100)
skf = StratifiedKFold(n_splits=CV,random_state=100,shuffle=True)
k = 0

S_metrics = "MEAN_SCORE\tMCC\tAUC\tSAMPLES\tGUILD\tYouden's index\n"

for guild in ParseTheseGuilds2:
    k += 1
    Y = []
    for i in range(len(y2)):
        if i in myguilds[guild]: #Change to myguilds2 if you want
            #Y.append(guild)
            Y.append(True)
        else:
            #Y.append("NOT "+ guild)
            Y.append(False)
    Y = numpy.array(Y, dtype=numpy.bool)


    #Increasing data size with SYNTH METAGENOMES (POSTIVES)
   # a = set(range(len(X)))
   # b = myguilds[guild]
   # c = a-b #Set with indices of NOT guild-members
   # xx = [] #
   # yy = [] #
   # for i in random.sample(b,random.randint(5,len(b))): #len(b) -1 was 5 ##REAL
   #     break
   # #for i in random.sample(b,random.randint(5,6)): #len(b) -1 was 5
   #     A = X[i,:].tolist()[0]
   #     noS = random.randint(int(len(c)/16),int(len(c)/8)) #No of NOT guild-members to merge with guild-member
   #     Aneg = [False for x in range(X.shape[1])]
   #     for ii in random.sample(c,noS):
   #         A = [  any(x) for x in zip(  A, X[ii,:].tolist()[0]     )          ]
   #     for ii in random.sample(c,noS):
   #         Aneg = [  any(x) for x in zip(  Aneg, X[ii,:].tolist()[0]     )          ]
   #     xx.append(A)
   #     yy.append(True)
   #     xx.append(Aneg)
   #     yy.append(False)
   # #xx = numpy.matrix(xx,dtype=bool)
   # #yy = numpy.array(yy,dtype=bool)

    #xx = numpy.concatenate((X,xx),0)
    #yy = numpy.concatenate((Y,yy),0)
    xx = X #UNCOMMENT AND COMMENT PREVIOUS 2 TO DISCARD FALSE METAGENOMES
    yy = Y
    #print(xx[-10].tolist()[0].count(True),xx[-20].tolist()[0].count(True),xx[-3].tolist()[0].count(True))
    YO.append(xx)

    scores = []
    Xmetascores = []
    mcc = []

    #ROC CURVE:
    #plt.figure(figsize=(10,10))
    plt.figure(figsize=(20,20))
    ax = plt.subplot(5,5,k)
    lw = 1
    tprs = []
    aucs = []
    mean_fpr = numpy.linspace(0, 1, 50)
    mean_J = []
    #for train, test in skf.split(X, Y):
    #for train, test in skf.split(D_X.toarray(), Y): #Feature hashing
    kk = 0
    for train, test in skf.split(xx, yy): #Increasing data size
        #X_train, X_test = X[train], X[test]
        #y_train, y_test = Y[train], Y[test]
        X_train, X_test = xx[train], xx[test] #increasing data size
        y_train, y_test = yy[train], yy[test] #increasing data size

        #RF = ensemble.ExtraTreesClassifier(n_estimators=10000,
        #                     max_features=128,
        #                     n_jobs=4,random_state=RFrandom_state)
        RF = ensemble.ExtraTreesClassifier(n_estimators=RFn_estimators,bootstrap=RFbootstrap,
                                    min_samples_split=RFmin_samples_split,max_features=RFmax_features,
                                    warm_start=RFwarm_start,max_depth=RFmax_depth,
                                    max_leaf_nodes=RFmax_leaf_nodes,n_jobs=RFn_jobs,random_state=RFrandom_state)




        #RF = ensemble.RandomForestClassifier(n_estimators=1000,max_features=32,oob_score=True,warm_start=True) # Creating the object. We use 50 trees
        #RF = ensemble.RandomForestClassifier(n_estimators=5000,max_features=64,warm_start=True,max_depth=6,max_leaf_nodes=6) # Creating the object. We use 50 trees
    ###        RF = ensemble.RandomForestClassifier(n_estimators=500,bootstrap=False,min_samples_split=2,
    ###                                    max_features=128,warm_start=True,max_depth=3,
    ###                                    max_leaf_nodes=6,n_jobs=8,random_state=10)
        #RFn_estimators
        RF = ensemble.RandomForestClassifier(n_estimators=10,bootstrap=RFbootstrap,
                                    min_samples_split=RFmin_samples_split,max_features=RFmax_features,
                                    warm_start=RFwarm_start,max_depth=RFmax_depth,min_samples_leaf=RFmin_samples_leaf,
                                    max_leaf_nodes=RFmax_leaf_nodes,n_jobs=RFn_jobs,random_state=RFrandom_state)
###
        #RF = svm.SVC(kernel=RFkernel, C=RFC, probability=RFprobability,
        #                   degree=RFdegree,random_state=RFrandom_state)

        #RF = svm.SVC()
        #RF = GaussianNB()
        #RF = MLPClassifier(solver='lbfgs', alpha=1e-5,
        #            hidden_layer_sizes=(100,2), random_state=1)

        ##One HOT ENCODING (LOOK AT LINE 116)
        #enc = OneHotEncoder(dtype=numpy.bool,sparse=False,n_values=2)
        #X_train = enc.fit_transform(X_train)
        #X_test = enc.fit_transform(X_test)

        ## ADDING JOKER!!!!!
        #X_joker = numpy.array([True for i in range(X.shape[1])]).reshape(1,-1)
        #X_joker.astype(bool)
        #X_train = numpy.concatenate((X_train,X_joker),0)
        #y_train = numpy.concatenate((y_train,numpy.array([True],dtype=numpy.bool)),0)

        #RF.fit(X_train[:,nitrogen_feat],y_train) # TOP 10 FEATURES, DELETE
        #y_pred = RF.predict(X_test[:,nitrogen_feat]) # TOP 10 FEATURES, DELETE, UNCOMMENT FOLLOWING 2 LINES
        RF.fit(X_train,y_train) #Fitting model on data training set
        y_pred = RF.predict(X_test) #Predictions on data test set
        #Getting accuracy for Logistic regression:
        #misclassrate = RF.score(X_test[:,nitrogen_feat], y_test) # TOP 10 FEATURES, DELETE, UNCOMMENT NEXT LINE
        misclassrate = RF.score(X_test, y_test)
        scores.append(round(misclassrate,2))
        #print(matthews_corrcoef(y_test,y_pred))
        #label_average_ranking_precision.append(label_ranking_average_precision_score(y_test, y_pred) )
        #cover_error.append(coverage_error(y_test, y_pred))
        #ranking_loss.append(label_ranking_loss(y_test, y_pred))
        #Xmetascores.append( RF.predict_proba(Xmeta[:,nitrogen_feat]).tolist() ) # TOP 10 FEATURES, DELETE, UNCOMMENT NEXT LINE
        #Xmetascores.append( RF.predict_proba(enc.fit_transform(Xmeta)).tolist() ) #ONE HOT ENCODING!!!!
        #######Xmetascores.append( RF.predict_proba(Xmeta).tolist() ) #NORMAL!!!

        y_pred2 = RF.predict_proba(X_test)[:,1]
        #AUC INSTEAD OF ACCURACY SCORES!!!!
        fpr, tpr, threshold = metrics.roc_curve(y_test, y_pred2,pos_label=True)
        roc_auc = metrics.auc(fpr, tpr)
        #print(roc_auc)
        mcc.append(matthews_corrcoef(y_test,y_pred)) #MATTHEWS COEFFICIENT

        tprs.append(interp(mean_fpr, fpr, tpr))
        tprs[-1][0] = 0.0
        aucs.append(roc_auc)
        #plt.plot(fpr, tpr, lw=lw, alpha=0.3,
        #label='ROC fold %d (AUC = %0.2f)' % (kk, roc_auc))

        #sup = scores.pop(-1)
        #scores.append(roc_auc)
        #plt.plot(fpr, tpr, color=NUM_COLORS_L[kk],
        #     lw=lw, label='ROC curve (area = %0.2f)' % roc_auc)
        kk += 1
        mean_J.append(round(get_threshold(fpr, tpr, threshold),2))
    #ROC CURVES

    mean_tpr = numpy.mean(tprs, axis=0)
    mean_tpr[-1] = 1.0
    mean_auc = metrics.auc(mean_fpr, mean_tpr)
    std_auc = numpy.std(aucs)
    plt.plot(mean_fpr, mean_tpr, color='b',
    label=r'Mean ROC (AUC = %0.2f $\pm$ %0.2f)' % (mean_auc, std_auc),
    lw=2, alpha=.8)

    std_tpr = numpy.std(tprs, axis=0)
    tprs_upper = numpy.minimum(mean_tpr + std_tpr, 1)
    tprs_lower = numpy.maximum(mean_tpr - std_tpr, 0)
    plt.fill_between(mean_fpr, tprs_lower, tprs_upper, color='grey', alpha=.2,
                     label=r'$\pm$ 1 std. dev.')
    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Mean ROC (AUC = {0} $\pm$ {1})\n{2}'.format(round(mean_auc,2), round(std_auc,2),guild))
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width, box.height*0.8])
    plt.legend(loc="lower right")
    #plt.show()
    plt.savefig("{0}plots/{1}.png".format(opath,guild))
    plt.close()

    mean_J_no = numpy.mean(numpy.array(mean_J))
    print("{0}:\t{1}\t{2}".format(round(mean_J_no,2),guild, [str(_)[0:3] for _ in mean_J]     ))
    mythresholds[guild] = mean_J_no
    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('(ROC): {0}'.format(guild))
    plt.legend(loc="lower right")
#    plt.show()
    plt.savefig("{0}plots/{1}.2.png".format(opath,guild))
    plt.close()
    #
    Tscores.extend(scores)
    scores = numpy.array(scores)
    mcc = numpy.array(mcc)
#    print("Tscores:\t{0}".format(Tscores))
    print("scores:\t{0}".format(scores))
    print("mcc:\t{0}".format(mcc))
    #print("{0}% (+/- {1}%) : {2} ({3})".format(round(scores.mean(),2), round(scores.std() * 2,2), guild ,scores))
    #print("{0} (+/- {1}) : {2} {3}".format(round(scores.mean(),4), round(scores.std() * 2,2), guild,scores))


    temp_metrics = "{0}\t{2}\t{7}\t{5}\t{4}\t{8}\n".format(round(scores.mean(),2),
                                    round(scores.std(),2),
                                    round(mcc.mean(),2),round(mcc.std(),2),guild,len(myguilds2[guild]),
                                    RFmax_features,
                                    round(mean_auc,2),
                                    round(mean_J_no,2)
                                    )
    print(temp_metrics)
    S_metrics += temp_metrics

#    Performances.append(  [mean_auc, mcc.mean() , guild  ]  )

#    tree after grid search
    RF = ensemble.RandomForestClassifier(n_estimators=RFn_estimators,bootstrap=RFbootstrap,
                                    min_samples_split=RFmin_samples_split,max_features=RFmax_features,
                                    warm_start=RFwarm_start,max_depth=RFmax_depth,
                                    max_leaf_nodes=RFmax_leaf_nodes,n_jobs=RFn_jobs,random_state=RFrandom_state)

    RF.fit(xx,yy) #UNCOMMENT + 2, NORMAL MODE
    ModelList.append(RF) #NORMAL
    
    importances = RF.feature_importances_
    std = numpy.std([tree.feature_importances_ for tree in RF.estimators_], axis=0)
    indices = numpy.argsort(importances)[::-1]
    # Print the feature ranking
#    print("Feature ranking: {0}".format(guild))
#    for f in range(10):
#        print("{0}. feature {1} {2} ---- {3} - {4}".format(f + 1, indices[f],
#                                                           #round(importances[indices[f]],10),
#                                                           importances[indices[f]],
#                                                           feature_names[0,indices[f]],feature_names_extended[0,indices[f]]    ))

    Guildindice.append(indices[range(100)])
    myr = 10
    # Plot the feature importances of the forest
    print("Guild: {0}\n******".format(k))

plt.savefig("{0}plots/{1}.png".format(opath,"dfsf"))
#plt.show()
print("Saving model as: {0}model.pkl".format(opath))
S_log += "Saving model as: {0}model.pkl\n".format(opath)
S_log += "{0}\n".format(ram_usage())
# Save model here......
from sklearn.externals import joblib
joblib.dump([feature_names,ParseTheseGuilds,ModelList],"{0}model.pkl".format(opath),compress=True)

writelog(S_log,"/log/log.txt",opath)
writelog(S_metrics,"metrics.txt",opath)


