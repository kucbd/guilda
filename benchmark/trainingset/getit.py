#PROTRAITS to final table
import sys
#1 ProTraits list
#2 Guildname

realD = {}
with open("pickles/51guilds.csv") as fid:
    line = fid.readline()
    line = fid.readline()
    while line:
        temp = []
        ls = line.rstrip().split(";")
        for el in range(0,len(ls[2:])):
            if "methan" == ls[el][0:6]:
                ls[el] = "methanogenesis"
                print(ls[el][0:6])
        realD[ls[0]] = ls[2:]
        line = fid.readline()


common = set()
with open("common.id") as f:
 for line in f:
  common.add(line.rstrip())

guild = " ".join(sys.argv[2:])
id2prob = {}
name2prob = {}
with open(sys.argv[1]) as fid:
 for line in fid:
  ls = line.split(";")
  prob = ls[-2]
  myid = ls[0]
  if prob  == "?":
   prob = 0
  id2prob[myid] = float(prob)

import pickle
#realD = pickle.load(open("realD.p","rb"))
TP,FP,TN,FN = 0,0,0,0
for mykey in common:
    if mykey not in id2prob and guild in realD[mykey]:
        FN += 1
    elif mykey not in id2prob and guild not in realD[mykey]:
        TN += 1
    elif id2prob[mykey] >=0.5 and guild in realD[mykey]:
        TP += 1
    elif id2prob[mykey] < 0.5 and guild not in realD[mykey]:
        TN += 1
    elif id2prob[mykey] >= 0.5 and guild not in realD[mykey]:
        FP += 1
    else:
        FN += 1

total = len(common)
correct = TP+TN
print("correct",correct,"Accuracy",100*correct/total)

print("TP",TP,"FP",FP,"TN",TN,"FN",FN,"TOTAL",total)
#print(TP,FP,TN,FN,total)
print("{0}\t{6}\t{1}\t{2}\t{3}\t{4}".format(guild,TP,FP,TN,FN,total, 100*correct/total   ))
