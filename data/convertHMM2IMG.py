import sys
#Adds new column with organism to existing abundance table

pfamF = sys.argv[1] #Pfam Abundance table from IMG (final_abundance.tab")
hmmF = sys.argv[2] #PFAM annotated file with HMM for new organisms
neworg = sys.argv[3] #Name of new organism

#d[x] = d.setdefault(x,0) + 1
d = {}
with open(pfamF) as f1, open(hmmF) as f2:
    for line in f2:
        if line[0] == "#":
            continue
        x = line.split()[3].split(".")[0]
        d[x] = d.setdefault(x,0) + 1

    s = ""
    header = f1.readline().rstrip()+"\t{0}\n".format(neworg)
    line = f1.readline()
    while line:
        pfam = line.split()[0]
        s += line.rstrip() + "\t{0}\n".format( d.setdefault(pfam,0)    )
        line = f1.readline()


with open("final_abundance.tab","w") as fout:
    fout.write(header+s)
